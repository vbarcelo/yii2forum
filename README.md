Yii 2 Forum
============================

Yii 2 Basic Forum. http://yii2forum.victorbarcelo.net 

---

**Features:**
- Login system with user registration.
- Voting system that allows upvoting and downvoting of both Threads and Posts.
- Sorting forum Threads.
- Form validation; allows content to have maximum of 100 characters and disallow URLs to be used in it (Posts and Threads).
