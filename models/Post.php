<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Content;
use app\models\PostUserVote;

/**
 * This is the model class for table "post".
 */
class Post extends Content
{
    /**
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $parentRules = parent::rules();
        $modelRules = [
        ];
        $rules = ArrayHelper::merge($parentRules, $modelRules);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $parentAttributeLabels = parent::attributeLabels();
        $modelAttributeLabels = [];
        $attributeLabels = ArrayHelper::merge($parentAttributeLabels, $modelAttributeLabels);
        return $attributeLabels;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorName()
    {
        return User::findOne(['id' => $this->id_user])->username;
    }

    public function getUserVote($userId)
    {
        $postUserVote = PostUserVote::find()
            ->where(['id_user' => $userId, 'id_post' => $this->id])
            ->one();
        if (!$postUserVote) return 0; else
            return $postUserVote->votecount;
    }

    public function vote($userId, $VoteInt)
    {
        $postUserVote = PostUserVote::find()
            ->where(['id_user' => $userId, 'id_post' => $this->id])
            ->one();
        if (!$postUserVote) {
            $postUserVote = new PostUserVote();
            $postUserVote->id_post = $this->id;
            $postUserVote->id_user = $userId;
            $postUserVote->votecount = $VoteInt;
            $postUserVote->save(false);
            $this->votingcount += $VoteInt;
            $this->save(false);
        } else {
            $postUserVoteCount = $postUserVote->votecount;
            if ($postUserVoteCount == 0 && $VoteInt == 1) {
                $postUserVote->votecount = 1;
                $this->votingcount += 1;
                $this->save(false);
                $postUserVote->save();
            }
            if ($postUserVoteCount == 0 && $VoteInt == -1) {
                $postUserVote->votecount = -1;
                $this->votingcount += -1;
                $this->save(false);
                $postUserVote->save();
            }
            if ($postUserVoteCount == 1 && $VoteInt == -1) {
                $postUserVote->votecount = -1;
                $this->votingcount += -2;
                $this->save(false);
                $postUserVote->save();
            }
            if ($postUserVoteCount == -1 && $VoteInt == 1) {
                $postUserVote->votecount = 1;
                $this->votingcount += 2;
                $this->save(false);
                $postUserVote->save();
            }
        }
    }
}