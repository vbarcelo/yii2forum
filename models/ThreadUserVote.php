<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "thread_uservote".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_thread
 * @property integer $votecount
 * @property User $idUser
 * @property Thread $idThread
 */
class ThreadUserVote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thread_uservote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_thread', 'votecount'], 'integer'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
            [['id_thread'], 'exist', 'skipOnError' => true, 'targetClass' => Thread::className(), 'targetAttribute' => ['id_thread' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_thread' => 'Id Thread',
            'votecount' => 'Votecount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdThread()
    {
        return $this->hasOne(Thread::className(), ['id' => 'id_thread']);
    }
}
