<?php

namespace app\models;

use Yii;

/**
 * @property integer $id
 * @property string $content
 * @property string $timestamp
 * @property integer $votingcount
 * @property string $authorName
 * @property captcha $captcha
 */
class Content extends \yii\db\ActiveRecord
{
    public function rules()
    {
        return [
            [
                'content',
                'string',
                'max' => 100
            ],
            [
                'content',
                'required'
            ],
            [
                'content',
                'match',
                'not' => true,
                'pattern' => "'(http|ftp|https):\/\/[\\w-]+(\\.[\\w-]+)+([\\w.,@?^=%&:\/~+#-]*[\\w@?^=%&\/~+#-])?'",
                'message' => 'The content must not contain URLs'
            ],
            [
                'content',
                'match',
                'not' => true,
                'pattern' => "'([^([:ascii:]))&(^[áéíóúñÑ¿¡ÁÉÍÓÚ])'",
                'message' => 'The content has invalid characters'
            ],
            ['captcha', 'required'],
            ['captcha', 'captcha']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content' => 'Content',
            'timestamp' => 'Creation date',
            'authorName' => 'Author',
            'votingcount' => 'Votes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorName()
    {
        return User::findOne(['id' => $this->id_user])->username;
    }
}