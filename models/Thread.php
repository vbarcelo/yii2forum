<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\ThreadUserVote;
use app\models\Post;
use app\models\Content;

/**
 * This is the model class for table "thread".
 *
 * @property string $title
 * @property integer $replies
 * @property Post[] $posts
 */
class Thread extends Content
{
    /**
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thread';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $parentRules = parent::rules();
        $modelRules = [
            ['title', 'string', 'max' => 100],
            ['title', 'required'],
        ];
        $rules = ArrayHelper::merge($parentRules, $modelRules);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $parentAttributeLabels = parent::attributeLabels();
        $modelAttributeLabels = ['title' => 'Title'];
        $attributeLabels = ArrayHelper::merge($parentAttributeLabels, $modelAttributeLabels);
        return $attributeLabels;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id_thread' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReplies()
    {
        $threadUserVotes = Post::find()
            ->where(['id_thread' => $this->id])
            ->count();
        return $threadUserVotes;
    }

    public function getUserVote($userId)
    {
        $threadUserVote = ThreadUservote::find()
            ->where(['id_user' => $userId, 'id_thread' => $this->id])
            ->one();
        if (!$threadUserVote) return 0; else
            return $threadUserVote->votecount;
    }

    public function vote($userId, $VoteInt)
    {
        $threadUserVote = ThreadUservote::find()
            ->where(['id_user' => $userId, 'id_thread' => $this->id])
            ->one();
        if (!$threadUserVote) {
            $threadUserVote = new ThreadUservote();
            $threadUserVote->id_thread = $this->id;
            $threadUserVote->id_user = $userId;
            $threadUserVote->votecount = $VoteInt;
            $threadUserVote->save();
            $this->votingcount += $VoteInt;
            $this->save(false);
        } else {
            $threadUserVoteCount = $threadUserVote->votecount;
            if ($threadUserVoteCount == 0 && $VoteInt == 1) {
                $threadUserVote->votecount = 1;
                $this->votingcount += 1;
                $this->save(false);
                $threadUserVote->save();
            }
            if ($threadUserVoteCount == 0 && $VoteInt == -1) {
                $threadUserVote->votecount = -1;
                $this->votingcount += -1;
                $this->save(false);
                $threadUserVote->save();
            }
            if ($threadUserVoteCount == 1 && $VoteInt == -1) {
                $threadUserVote->votecount = -1;
                $this->votingcount += -2;
                $this->save(false);
                $threadUserVote->save();
            }
            if ($threadUserVoteCount == -1 && $VoteInt == 1) {
                $threadUserVote->votecount = 1;
                $this->votingcount += 2;
                $this->save(false);
                $threadUserVote->save();
            }
        }
    }
}
