<?php

//// comment out the following lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
//defined('YII_ENV') or define('YII_ENV', 'test');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

if (YII_ENV_TEST) {
    if (!in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
        die('You are not allowed to access this file.');
    }
    $config = require(__DIR__ . '/../config/test.php');
} else {
    $config = require(__DIR__ . '/../config/web.php');
}

(new yii\web\Application($config))->run();
