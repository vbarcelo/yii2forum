/*
Navicat MySQL Data Transfer

Source Server         : vto.do (master)
Source Server Version : 50550
Source Host           : oceanvto.duckdns.org:3306
Source Database       : yii2_forum

Target Server Type    : MYSQL
Target Server Version : 50550
File Encoding         : 65001

Date: 2017-04-02 17:52:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1490296289');
INSERT INTO `migration` VALUES ('m150214_044831_init_user', '1490296294');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_thread` int(11) DEFAULT NULL,
  `content` varchar(100) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `votingcount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_post_user` (`id_user`),
  KEY `FK_post_thread` (`id_thread`),
  CONSTRAINT `FK_post_thread` FOREIGN KEY (`id_thread`) REFERENCES `thread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_post_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of post
-- ----------------------------

-- ----------------------------
-- Table structure for post_uservote
-- ----------------------------
DROP TABLE IF EXISTS `post_uservote`;
CREATE TABLE `post_uservote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_post` int(11) DEFAULT NULL,
  `votecount` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_uservote_user` (`id_user`),
  KEY `FK_post_uservote_post` (`id_post`),
  CONSTRAINT `FK_post_uservote_post` FOREIGN KEY (`id_post`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_post_uservote_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of post_uservote
-- ----------------------------

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_user_id` (`user_id`),
  CONSTRAINT `profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profile
-- ----------------------------
INSERT INTO `profile` VALUES ('1', '1', '2017-03-23 19:11:34', '2017-03-23 19:48:38', 'Víctor Barceló', null);
INSERT INTO `profile` VALUES ('3', '3', '2017-03-24 20:32:59', '2017-03-24 20:32:59', 'pepito jeje', null);
INSERT INTO `profile` VALUES ('4', '4', '2017-03-25 12:25:18', '2017-03-25 12:26:22', 'Test user', 'Pacific/Midway');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `can_admin` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin', '2017-03-23 19:11:33', null, '1');
INSERT INTO `role` VALUES ('2', 'User', '2017-03-23 19:11:33', null, '0');

-- ----------------------------
-- Table structure for thread
-- ----------------------------
DROP TABLE IF EXISTS `thread`;
CREATE TABLE `thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(100) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `viewcount` int(11) DEFAULT '0',
  `votingcount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_thread_user` (`id_user`),
  CONSTRAINT `FK_thread_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of thread
-- ----------------------------

-- ----------------------------
-- Table structure for thread_uservote
-- ----------------------------
DROP TABLE IF EXISTS `thread_uservote`;
CREATE TABLE `thread_uservote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_thread` int(11) DEFAULT NULL,
  `votecount` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_thread_uservote_thread` (`id_thread`),
  KEY `FK_thread_uservote_user` (`id_user`),
  CONSTRAINT `FK_thread_uservote_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_thread_uservote_thread` FOREIGN KEY (`id_thread`) REFERENCES `thread` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of thread_uservote
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged_in_at` timestamp NULL DEFAULT NULL,
  `created_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  `banned_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`email`),
  UNIQUE KEY `user_username` (`username`),
  KEY `user_role_id` (`role_id`),
  CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', 'vbarcelo@gmail.com', 'vbarcelo', '$2y$13$awVL7jcea19YlIIhGGr8MOEbAvMlzYV5y2RRBx8Jnp.BwlBVP6xBe', 'dpovu2BmnHUk1Hbq6MVff6iLolpWnyk0', 'wwCbEbkFaRQcviFJLiiaZsJyctR23Yaa', '127.0.0.1', '2017-04-02 16:47:41', null, '2017-03-23 19:11:33', '2017-03-25 20:38:50', null, null);
INSERT INTO `user` VALUES ('3', '2', '1', 'vbarcelo@mmi-e.com', 'pepito', '$2y$13$oSKJlwmo6/rDS0R7ufIHdOEi53ociH0XOjmW7RXZpP9ZQqRpZTcOm', null, null, '127.0.0.1', '2017-03-24 20:33:17', null, '2017-03-24 20:32:59', '2017-03-24 20:32:59', null, null);
INSERT INTO `user` VALUES ('4', '2', '1', 'yii2forum@gmail.com', 'someguy', '$2y$13$j72mIXcujKTa3/e2CtKyluDdgserc7Q9Fayr/fZyUGfKutUpxPZH.', 'H8dZfDjDYP1Ut738DIZPlSxCxdgHWfeK', 'OrctAd5Ilx14X3cfMqWEFv3Iy8S9xm7z', '80.26.88.203', '2017-03-28 18:22:01', '127.0.0.1', '2017-03-25 12:25:18', '2017-03-27 14:54:03', null, null);

-- ----------------------------
-- Table structure for user_auth
-- ----------------------------
DROP TABLE IF EXISTS `user_auth`;
CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_attributes` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_auth_provider_id` (`provider_id`),
  KEY `user_auth_user_id` (`user_id`),
  CONSTRAINT `user_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_auth
-- ----------------------------

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `expired_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_token_token` (`token`),
  KEY `user_token_user_id` (`user_id`),
  CONSTRAINT `user_token_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_token
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
