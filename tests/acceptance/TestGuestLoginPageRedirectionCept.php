<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that guest user is redirected to login page');
$I->amOnPage('');
$I->see('Login');