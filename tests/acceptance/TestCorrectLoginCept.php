<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that correct login/password will redirect to main page (threadView as root path)');
$I->amOnPage('');
$I->fillField('input[name="LoginForm[email]"]', 'vbarcelo');
$I->fillField('input[name="LoginForm[password]"]', 'foo');
$I->click('button[type=submit]');
$I->see('Welcome to the Yii2 Forum!');