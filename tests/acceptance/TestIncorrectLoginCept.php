<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that incorrect login/password will throw error');
$I->amOnPage('');
$I->fillField('input[name="LoginForm[email]"]', 'vbarcelo');
$I->fillField('input[name="LoginForm[password]"]', 'foo2');
$I->click('button[type=submit]');
$I->see('Incorrect Password');