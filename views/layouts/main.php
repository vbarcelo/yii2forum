<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Yii2 Forum',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $navBarGuestItems = [['label' => 'Login', 'url' => ['/user/login']]];

            $navBarUserItems = [
                ['label' => 'Profile', 'url' => ['/user/profile']],
                ['label' => 'Account', 'url' => ['/user/account']],
                [
                    'label' => 'Logout (' . Yii::$app->user->displayName . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ]
            ];

            $navBarAdminItems = [
                ['label' => 'Profile', 'url' => ['/user/profile']],
                ['label' => 'Account', 'url' => ['/user/account']],
                [
                    'label' => 'Users Management',
                    'url' => ['/user/admin']
                ],
                [
                    'label' => 'Logout (' . Yii::$app->user->displayName . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ]
            ];

            if (Yii::$app->user->isGuest) {
                $navBarItems = $navBarGuestItems;
            } else if (!Yii::$app->user->can("admin")) {
                $navBarItems = $navBarUserItems;
            } else if (Yii::$app->user->can("admin")) {
                $navBarItems = $navBarAdminItems;
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $navBarItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">Yii2 Forum <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
