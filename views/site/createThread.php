<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model app\models\Thread */
/* @var $form ActiveForm */
?>

<div class="createThread">
    <?php if (!$canCreateContent): ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            Sorry, the creation time between new content must be greater
            than <?= Html::encode($secondsBeforeContentCreation) ?> seconds. Please try later.
        </div>
        <?= Html::a('Return to forum index', [Url::to(['/'])], [
        'class' => 'btn btn-primary']) ?>
        <?php else: ?>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'content')->textarea() ?>
        <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::className()) ?>
        <div class="form-group">
            <?= Html::submitButton('Create new Thread', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Cancel', [Url::to(['/'])], [
                'class' => 'btn btn-danger grid-button'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    <?php endif; ?>
</div>