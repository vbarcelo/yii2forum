<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

?>

<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
<div class="container">
    <?= Html::a('Return to Forum index', [Url::to(['/'])], [
        'class' => 'btn btn-primary grid-button',
        'style' => 'margin-bottom:10px'
    ]) ?>
    <div class="well well-sm" style="background: rgba(255,225,175,0.09)">
        <div class="media">
            <div class="media-body">
                <h3 class="media-heading">Title: <?= Html::encode($thread->title) ?></h3>
                <p class="text-right"><i class="glyphicon glyphicon-user"></i>
                    By <?= Html::encode($thread->authorname) ?>
                </p>
                <p><?= Html::encode($thread->content) ?></p>
                <ul class="list-inline list-unstyled">
                    <li>
                        <span><i class="glyphicon glyphicon-calendar"></i> <?= Html::encode($thread->timestamp) ?></span>
                    </li>
                    <li>|</li>
                    <span><i class="glyphicon glyphicon-eye-open"></i> Views: <?= Html::encode($thread->viewcount) ?> </span>
                    <li>|</li>
                    <span><i class="glyphicon glyphicon-heart-empty"></i> Votes: <?= Html::encode($thread->votingcount) ?> </span>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <li>|</li>
                        <span><?= Html::a('UpVote', [Url::to(['site/thread-up-vote', 'threadId' => $thread->id])], [
                                'class' => 'btn btn-primary grid-button btn-xs'
                            ]) ?>
                            <?= Html::a('DownVote', [Url::to(['site/thread-down-vote', 'threadId' => $thread->id])], [
                                'class' => 'btn btn-primary grid-button btn-xs'
                            ]) ?></span>
                        <li>|</li>
                        <span> Your current vote: <?= Html::encode($thread->getUserVote(Yii::$app->user->id)) ?> </span>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>


    <?php if (count($posts) === 0): ?>
        <h4>No Replies yet.</h4>
    <?php else: ?>
        <h4>Replies:</h4>
    <?php endif; ?>


    <?php foreach ($posts as $post): ?>
        <div class="well well-sm" style="background: rgba(216,132,186,0.09)">
            <p class="text-right"><i class="glyphicon glyphicon-user"></i>
                By <?= Html::encode($post->authorname) ?>
            </p>
            <p><?= Html::encode($post->content) ?></p>
            <ul class="list-inline list-unstyled">
                <li>
                    <span><i class="glyphicon glyphicon-calendar"></i> <?= Html::encode($post->timestamp) ?></span>
                </li>
                <li>|</li>
                <span><i class="glyphicon glyphicon-heart-empty"></i> Votes: <?= Html::encode($post->votingcount) ?> </span>
                <?php if (!Yii::$app->user->isGuest): ?>
                    <li>|</li>
                    <span><?= Html::a('UpVote', [Url::to(['site/post-up-vote', 'postId' => $post->id])], [
                            'class' => 'btn btn-primary grid-button btn-xs',
                            'style' => ''
                        ]) ?>
                        <?= Html::a('DownVote', [Url::to(['site/post-down-vote', 'postId' => $post->id])], [
                            'class' => 'btn btn-primary grid-button btn-xs',
                            'style' => ''
                        ]) ?></span>
                    <li>|</li>
                    <span> Your current vote: <?= Html::encode($post->getUserVote(Yii::$app->user->id)) ?> </span>
                <?php endif; ?>
            </ul>
        </div>
    <?php endforeach; ?>

    <!--Only registered users can reply in a Thread-->
    <?php if (!Yii::$app->user->isGuest): ?>
        <?php if (!$canCreateContent): ?>
            <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                You must wait <?= Html::encode($secondsBeforeContentCreation) ?> seconds before creating new content.
            </div>
        <?php else: ?>
            <div class="viewThread">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($newPost, 'content')->textarea()->label('Reply') ?>
                <?= $form->field($newPost, 'captcha')->widget(\yii\captcha\Captcha::className()) ?>
                <div class="form-group">
                    <?= Html::submitButton('Reply', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

</div>