<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use fedemotta\datatables\DataTables;
?>

<h1>Welcome to the Yii2 Forum!</h1>

<!--Only registered users can create a new Thread-->
<?php
if (!Yii::$app->user->isGuest) {
    echo Html::a('Create new Thread', [Url::to(['thread/create'])], [
        'class' => 'btn btn-primary grid-button',
        'id' => 'create-thread-btn',
        'style' => 'margin-bottom:10px'
    ]);
}
?>

<div class="well well-sm">
    <?= DataTables::widget([
        'dataProvider' => $provider,
        //    'filterModel' => $threads,
        'clientOptions' => [
            'lengthChange' => false,
            'paging' => false,
            'order' => [2, 'desc']
        ],
        'columns' => [
            [
                'label' => 'Title',
                'format' => 'raw',
                'value' => function ($thread) {
                    return Html::a("{$thread->title}", Url::to([
                        'site/view-thread',
                        'id' => $thread->id,
                    ]));
                        //['onclick' => "incrementThreadViewCount($thread->id)"]);
                },
            ],
            'authorname',
            'timestamp',
            [
                'label' => 'Views',
                'format' => 'raw',
                'value' => function ($thread) {
                    $html = Html::encode($thread->viewcount);
                    return $html;
                },
            ],
            [
                'label' => 'Votes',
                'format' => 'raw',
                'value' => function ($thread) {
                    $html = Html::encode($thread->votingcount);
                    return $html;
                },
            ],
            [
                'label' => 'Replies',
                'format' => 'raw',
                'value' => function ($thread) {
                    $html = Html::encode($thread->replies);
                    return $html;
                },
            ],
        ],
    ]); ?>
</div>

<script>
    function incrementThreadViewCount(threadId) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . 'thread/incrementviewcount/'?>' + threadId,
            data: {threadId: threadId},
            type: "GET",
            success: function () {
            },
            error: function () {
            }
        });
    }
</script>