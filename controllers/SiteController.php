<?php

namespace app\controllers;

use app\models\Thread;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\Post;
use yii\db\Expression;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{

    public function actionListThreads()
    {
        $query = Thread::find();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                    'title' => SORT_ASC,
                ]
            ],
        ]);
        return $this->render('threadList', [
            'provider' => $provider,
        ]);
    }

    public function actionCreateThread()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(array('/user/login'));
        }
        $newThread = new Thread;
        if ($newThread->load(Yii::$app->request->post())) {
            if ($newThread->validate()) {
                $newThread->timestamp = new Expression('NOW()');
                $newThread->id_user = Yii::$app->user->id;
                //TODO: Make captcha work with serverside validation: https://github.com/yiisoft/yii2/issues/6115
                if ($newThread->save(false)) {
                    $this->setContentCreationDateTime();
                    $this->redirect('/');
                }
                return;
            }
        }
        $secondsBeforeContentCreation = 30;
        return $this->render('createThread', [
            'model' => $newThread,
            'canCreateContent' => $this->canCreateContent($secondsBeforeContentCreation),
            'secondsBeforeContentCreation' => $secondsBeforeContentCreation
        ]);
    }

    public function actionViewThread($id)
    {
        $thread = Thread::find()->where(['id' => $id])->one();
        //Allow view counter to be increased by one user maximum once per 15 minutes.
        $allowedMinutesBeforeViewIncrement = 15;
        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();
        if (!isset($_SESSION['threadViewDateTimes'][$id])) {
            $_SESSION['threadViewDateTimes'][$id] = ['lastTimeVisited' => (new \DateTime())->format('Y-m-d H:i:s')];
            $thread->updateCounters(['viewcount' => 1]);
        } else {
            $now = (new \DateTime());
            $lastVisit = (new \DateTime())->createFromFormat('Y-m-d H:i:s', $_SESSION['threadViewDateTimes'][$id]['lastTimeVisited']);
            $interval = date_diff($now, $lastVisit);
            Yii::info($interval->format('%i'));
            if ($interval->format('%i') > $allowedMinutesBeforeViewIncrement) $thread->updateCounters(['viewcount' => 1]);
            $_SESSION['threadViewDateTimes'][$id] = ['lastTimeVisited' => (new \DateTime())->format('Y-m-d H:i:s')];
        }

        $newPost = new Post;
        if ($newPost->load(Yii::$app->request->post())) {
            if ($newPost->validate()) {
                $newPost->timestamp = new Expression('NOW()');
                $newPost->id_thread = $thread->id;
                $newPost->id_user = Yii::$app->user->id;
                //TODO: Make captcha work with serverside validation: https://github.com/yiisoft/yii2/issues/6115
                if ($newPost->save(false)) {
                    $this->setContentCreationDateTime();
                    $this->redirect("/thread/{$id}");
                } else {
                    Yii::info("");
                }
            }
        }
        $posts = $thread->getPosts()->all();
        $secondsBeforeContentCreation = 30;
        return $this->render('viewThread', [
            'thread' => $thread,
            'posts' => $posts,
            'newPost' => $newPost,
            'canCreateContent' => $this->canCreateContent($secondsBeforeContentCreation),
            'secondsBeforeContentCreation' => $secondsBeforeContentCreation
        ]);
    }

    public function setContentCreationDateTime()
    {
        $_SESSION['lastContentCreationDateTime'] = (new \DateTime())->format('Y-m-d H:i:s');
    }

    public function canCreateContent($secondsBeforeContentCreation)
    {
        $session = Yii::$app->session;
        if (!$session->isActive) $session->open();
        if (!isset($_SESSION['lastContentCreationDateTime'])) {
            $_SESSION['lastContentCreationDateTime'] = '';
            return true;
        } else {
            if($_SESSION['lastContentCreationDateTime'] == '') return true;
            $now = (new \DateTime());
            $lastContentCreationDateTime = (new \DateTime())->createFromFormat('Y-m-d H:i:s', $_SESSION['lastContentCreationDateTime']);
            $interval = date_diff($now, $lastContentCreationDateTime);
            Yii::info($interval->format('%s'));
            if ($interval->format('%s') > $secondsBeforeContentCreation) return true;
        }
    }

    public function actionThreadUpVote($threadId)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(array('/user/login'));
        }
        $thread = Thread::find()->where(['id' => $threadId])->one();
        $thread->vote(Yii::$app->user->id, 1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionThreadDownVote($threadId)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(array('/user/login'));
        }
        $thread = Thread::find()->where(['id' => $threadId])->one();
        $thread->vote(Yii::$app->user->id, -1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPostUpVote($postId)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(array('/user/login'));
        }
        $post = Post::find()->where(['id' => $postId])->one();
        $post->vote(Yii::$app->user->id, 1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPostDownVote($postId)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(array('/user/login'));
        }
        $post = Post::find()->where(['id' => $postId])->one();
        $post->vote(Yii::$app->user->id, -1);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
//            'captcha' => [
//                'class' => 'app\models\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}